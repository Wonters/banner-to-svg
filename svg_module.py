"""
This script add a banner with text and logo in png to an existing SVG
Developpement for SYNERGIE-WEB SYNAPS PLUGIN
"""

import svgwrite
import os
from bs4 import BeautifulSoup
import json
import base64


def create_banner(text: list, name: str, **kwargs):
    """
    Handle to create a cartouche with a rectangular shape
    ________________________
    | rect1 | rect2        |
    | text  |text/logo.png |
    |_______|______________|
    """
    banner_config = kwargs.get('banner_config', {
        'position': (0, 0),
        'height': 120,
        'fill': 'white',
        'stroke-width': 3,
        'stroke': 'black',
        'logo': {
            'height': 50,
            'width': 50,
            'position': ('85%', 20),
            'href': ''
        },
        'text': {
            'color': 'red',
            'line_space': 5,
            'justify': 4  # Split the sentence each 2 words
        }
    })

    dim_col = 100 / len(text)
    svg = svgwrite.Drawing(f'{name}.svg', size=('100%', banner_config['height']), profile='tiny')
    banner = svg.add(svg.g(id='banner', fill=banner_config['fill']))

    for i, v in enumerate(text):
        banner.add(svg.rect(insert=(f'{dim_col * i}%', 0),
                            id=f'rect{i}',
                            size=("100%", banner_config['height']),
                            stroke=banner_config['stroke'],
                            stroke_width=banner_config['stroke-width']))
        add_text(_text=text[i], pos=(f'{1 + dim_col * i}%', 20), _banner=banner, svg=svg,
                 color=banner_config['text']['color'],
                 line_space=banner_config['text']['line_space'],
                 justify=banner_config['text']['justify'])

    banner.add(
        svg.image(href=banner_config['logo']['href'], insert=banner_config['logo']['position'],
                  width=banner_config['logo']['width'],
                  height=banner_config['logo']['height']))
    return svg


def add_text(_text: str, pos: tuple, svg: svgwrite.Drawing, _banner, color: str, line_space: int,
             justify: int):
    """
    Add  justify and center text to a banner in the column
    """
    words = _text.split(" ")
    text_justify = [words[x:x + justify] for x, v in enumerate(words) if not x % justify]
    for i, line in enumerate(text_justify):
        _banner.add(svg.text(" ".join(line),
                             insert=(pos[0], pos[1] + i * line_space),
                             fill=color))


def add_banner_to_svg(svg_path: str, banner):
    """
    Add to a banner an svg image map
    """

    with open(svg_path) as f:
        svg_map = BeautifulSoup(markup=f.read(), features="xml")

    banner.add(
        banner.g(id="svg_map", transform=f'translate({0} {int(banner.attribs["height"]) + 5})'))
    banner.attribs['width'] = svg_map.find('svg')['width']
    banner.attribs['height'] = int(banner.attribs['height']) + int(
        svg_map.find('svg')['height'])
    final_svg = BeautifulSoup(markup=banner.tostring(), features="xml")
    group_svg_map = final_svg.find('g', {'id': 'svg_map'})
    group_svg_map.append(svg_map)
    with open(banner.filename, 'w') as f_svg:
        f_svg.write(str(final_svg.contents[0]))


""" Exemple of use """

if __name__ == "__main__":
    # specify the svg to add the banner to.
    svg_path = os.path.abspath(os.path.join("/".join(__file__.split('/')[:-1]),
                                            "sigwx.svg"))

    logo_path = os.path.abspath(os.path.join("/".join(__file__.split('/')[:-1]),
                                            "ogc.png"))

    # open banners file which store text
    banners_path = 'banners.json'
    with open(banners_path) as f:
        banners = json.loads(f.read())
    # select the banner desire
    sigwx_high = banners['SIGWX-HIGH']

    # Encode PNG in base64
    with open(logo_path, 'rb') as png:
        encode_png = base64.b64encode(png.read())

    # configure the banner
    banner_config = {
        'position': (0, 0),
        'height': 120,
        'fill': 'white',
        'stroke-width': 3,
        'stroke': 'black',
        'logo': {
            'height': 50,
            'width': 50,
            'position': ('85%', 20),
            'href': f'data:image/png;base64,{encode_png.decode()}'
        },
        'text': {
            'color': 'black',
            'line_space': 15,
            'justify': 8  # Split the sentence each 8 words
        }
    }

    banner = create_banner(text=sigwx_high['text'], name='banner', banner_config=banner_config)
    add_banner_to_svg(svg_path=svg_path, banner=banner)
